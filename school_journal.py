import telebot
from telebot import types
from telebot import apihelper
import pymongo
from datetime import datetime, timedelta
from settings import proxy, token

apihelper.proxy = proxy  #без прокси боты в России не работают, адрес взяла в бесплатном сервисе :(
bot = telebot.TeleBot(token) #токен бота

connection = pymongo.MongoClient() #подключаемся к БД
db = connection['school']
my_schedule = db['schedule']
my_homework = db['homework']


@bot.message_handler(commands=['start'])  #обработка события /start
def _start_message(message):
    bot.send_message(message.chat.id, 'Привет')  #отправка приветственного собщения для нового пользователя


@bot.message_handler(commands=['newhomework']) #обработка события /newhomework (добавление новой домашки)
def _select_day(message):  # функция выводит кнопки с днями недели и датами
    buttons = _generate_buttons()
    selected_day = bot.send_message(message.chat.id, 'Выбери день недели:', reply_markup=buttons)
    bot.register_next_step_handler(selected_day, _select_subject)


def _generate_buttons():  #функция генерирует массив "кнопок" с названием дня и датой. кнопок 5 - на ближайшие 5 рабочих (!) дней
    days = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ']
    current_day_idx = datetime.today().weekday()
    day_idxs = list()
    for i in range(0, len(days)):
        current_day_idx += 1
        day_idxs.append(current_day_idx % len(days))
    days_menu = types.ReplyKeyboardMarkup()
    day_buttons = []
    _date = datetime.now()
    for idx in day_idxs:
        _date += timedelta(days=1)
        day_buttons.append(days[idx]+' '+str(_date.date()))
    days_menu.add(*day_buttons)
    return days_menu


def _select_subject(message):  # функция выбора предмета: запрашивает расписание нужного дня и выводит в виде кнопок
    select_day = message.text[:message.text.index(" ")]
    select_data = message.text[message.text.index("2"):]
    dict_for_insert = dict()
    dict_for_insert['day'] = select_day
    dict_for_insert['date'] = select_data
    subjects_menu = types.ReplyKeyboardMarkup()
    for subject in my_schedule.find({"day": select_day}):
        subject_buttons = tuple(subject['subjects'])
        subjects_menu.add(*subject_buttons)
    selected_subject = bot.send_message(message.chat.id, 'Выбери предмет:', reply_markup=subjects_menu)
    bot.register_next_step_handler(selected_subject, _input_homework, dict_for_insert)


def _input_homework(message, dict_for_insert):  # функция предлагает ввести домашку и передет полученное значение в следующую функцию
    select_subject = message.text
    dict_for_insert['subject'] = select_subject
    homework = bot.send_message(message.chat.id, 'Введи домашку')
    bot.register_next_step_handler(homework, _save_homework, dict_for_insert)


def _save_homework(message, dict_for_insert):  # сохранение домашки в БД
    homework = message.text
    dict_for_insert['homework'] = homework
    bot.send_message(message.chat.id, 'Домашка добавлена')
    my_homework.save(dict_for_insert)


@bot.message_handler(commands=['myhomework'])  # обработка команды /myhomework (вывод домашки)
def _select_day(message):  # функция вывода кнопок с днями недели
    buttons = _generate_buttons()
    selected_day = bot.send_message(message.chat.id, 'Выбери день недели:', reply_markup=buttons)
    bot.register_next_step_handler(selected_day, _view_homework)


def _view_homework(message):  # функция вывода домашки из БД
    select_data = message.text[message.text.index("2"):]
    bot.send_message(message.chat.id, "Домашка на " + select_data + ":")
    for item in my_homework.find({"date": select_data}):
        bot.send_message(message.chat.id, item["subject"] + ": " + item["homework"])  # пыталась написать через форматирование, но bot.send_message ру


@bot.message_handler(commands=['myshedule'])  # обработка команды /myshedule  (вывод расписания)
def _select_day(message):
    buttons = _generate_buttons()
    selected_day = bot.send_message(message.chat.id, 'Выбери день недели:', reply_markup=buttons)
    bot.register_next_step_handler(selected_day, _view_schedule)


def _view_schedule(message):  # функция вывода расписания из БД
    select_day = message.text[:message.text.index(" ")]
    bot.send_message(message.chat.id, "Расписание на " + select_day + ":")
    for item in my_schedule.find({"day": select_day}):
        subject_buttons = item['subjects']
        subject = 0
        while subject < len(subject_buttons):
            bot.send_message(message.chat.id, str(subject+1) + ": " + subject_buttons[subject])
            subject +=1


if __name__ == '__main__':
    bot.polling(none_stop=True)